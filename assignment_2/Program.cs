﻿using System;

namespace assignment_2
{
    class Program
    {
        static void Main(string[] args)
        {

            int height = UserInput("Enter the size of the height you want to draw");
            Console.WriteLine("");
            int width = UserInput("Enter the size of the width you want to draw");
            Console.WriteLine("");
            DrawSquareRectangle(height, width);
        }

        public static int UserInput(string userChoice)
        {

            bool isValid = false;
            int size = 5;

            while (!isValid)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write($"{userChoice}: ");

                try
                {

                    size = Convert.ToInt32(Console.ReadLine());
                    isValid = true;

                    if (size <= 0)
                    {
                        isValid = false;
                        throw new ArgumentOutOfRangeException();
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"You have entered the invalid input. (Error: {ex.Message})");
                }
            }

            return size;
        }

        public static void DrawSquareRectangle(int height, int width)
        {
            Console.Write("you have entered: ");
            if (width == height)
            {
                Console.WriteLine("Square\n");
            }
            else
            {
                Console.WriteLine("Rectangle\n");
            }
            for (int i = 0; i < height; i++) // to draw the rows of the square/ rectangle 
            {
                Console.Write("*"); // to draw the first character 
                for (int j = 1; j < (width - 1); j++) // to draw the columns of the square/rectangle
                {
                    if (i == 0 || i == (height - 1)) // if the row is between 0 and the last number - 1, then fill in the specific character, else leave it 
                    {
                        Console.Write(" *");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine(" *");
            }
        }

    }
}
